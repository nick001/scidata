var send = function() {
    var user_name = $("#user_name").val();
    var secret_answer = $("#secret_answer").val();
    var data = {
        user_name:user_name,
        secret_answer:secret_answer
    };
    send_data_to(data, "/restore_password/");
};

var getSecretQuestion = function() {
    $("#user").hide();
    $("#answer").show();
    var user_name = $("#user_name").val();
    var data = {
        user_name:user_name
    };
    send_data_to(data, "/restore_password/");
};