var send = function() {
    var user_name = $("#user_name").val();
    var password = $("#password").val();
    var secret_question = $("#secret_question").val();
    var secret_answer = $("#secret_answer").val();
    var data = {
        user_name:user_name,
        password:password,
        secret_question:secret_question,
        secret_answer:secret_answer
    };
    send_data_to(data, "/registration/");
};