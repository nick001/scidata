function send_data_to(data, url){
  $.ajax
    ({
        type     : "POST",
        url      : url,
        data     : data,
        success  : function(response)
        {
        if(response) {
            var redirect_url = JSON.parse(response).redirect_url;
            var error_message = JSON.parse(response).error_message;
            console.log('in send: ' + redirect_url + ' ' + error_message);
            if (redirect_url) {
                location.href = redirect_url;
            }
            if (error_message){
                $('#error_message').html(error_message.replace("']", "").replace("['", "").replace("[]", ""));
            }
        }
        else
            console.log('in send:pass((');
        }
    });
}