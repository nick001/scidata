from django.db import models


class User(models.Model):
    user_name = models.CharField(max_length=40)
    password = models.CharField(max_length=40)
    secret_question = models.CharField(max_length=200)
    secret_answer = models.CharField(max_length=40)