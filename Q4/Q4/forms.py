from django import forms


class Login(forms.Form):
    user_name = forms.CharField(max_length=40)
    password = forms.CharField(max_length=40)


class Registration(forms.Form):
    user_name = forms.CharField(max_length=40)
    password = forms.CharField(max_length=40)
    secret_question = forms.CharField(max_length=200)
    secret_answer = forms.CharField(max_length=40)