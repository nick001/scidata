from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

from Q4.forms import Login, Registration
from Q4.models import User


@csrf_exempt
def registration(request):
    if request.method == 'POST' and request.is_ajax():
        form = Registration(request.POST)
        if form.is_valid():
            user_name = form.cleaned_data['user_name']
            password = form.cleaned_data['password']
            secret_question = form.cleaned_data['secret_question']
            secret_answer = form.cleaned_data['secret_answer']

            if User.objects.filter(user_name=user_name):
                return JsonResponse(
                    {'error_message': 'user name already exsist'}
                )

            new_user = User(
                user_name=user_name, password=password,
                secret_question=secret_question, secret_answer=secret_answer,
            )
            new_user.save()

            return JsonResponse({'redirect_url': '/home'})

    template = loader.get_template('registration.html')
    context = {'form': Registration()}
    return HttpResponse(template.render(context))


@csrf_exempt
def login(request):
    if request.method == 'POST' and request.is_ajax():
        form = Login(request.POST)
        if form.is_valid():
            user_name = form.cleaned_data['user_name']
            password = form.cleaned_data['password']

            if User.objects.filter(user_name=user_name, password=password):
                return JsonResponse({'redirect_url': '/home'})
            else:
                return JsonResponse(
                    {'error_message': 'wrong user name or password'}
                )

    template = loader.get_template('login.html')
    context = {'form': Login()}

    return HttpResponse(template.render(context))


@csrf_exempt
def restore_password(request):
    template = loader.get_template('restore_password.html')

    if request.method == 'POST' and request.is_ajax():
        user_to_restore = User.objects.filter(
            user_name=request.POST.get('user_name')
        ).first()
        secret_answer = request.POST.get('secret_answer')
        if not user_to_restore:
            return JsonResponse(
                {'error_message': 'wrong user name'}
            )
        elif not secret_answer:
            user_secret_question = user_to_restore.secret_question
            return JsonResponse(
                {'error_message': user_secret_question}
            )
        elif user_to_restore.secret_answer == secret_answer:
            restored_password_message = 'Your password is {}'.format(
                user_to_restore.password
            )
            return JsonResponse(
                {'error_message': restored_password_message}
            )
        elif user_to_restore.secret_answer != secret_answer:
            return JsonResponse(
                {'error_message': 'Really?'}
            )

    context = {}
    return HttpResponse(template.render(context, request))


def home(request):
    python_links = {
        'Программирование на Python. Том 1': 'https://www.ozon.ru/context/detail/id/8382738/',
        'Программирование на Python. Том 2': 'https://www.ozon.ru/context/detail/id/8382719/',
        'Python. К вершинам мастерства': 'https://www.ozon.ru/context/detail/id/135305378/',
        'Чистый код: создание, анализ и рефакторинг': 'https://www.ozon.ru/context/detail/id/21916535/'
    }
    template = loader.get_template('home.html')
    context = {'my_favourite_books_links': python_links}

    return HttpResponse(template.render(context, request))