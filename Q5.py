from urllib.request import urlopen


def get_page(url):
    """
    Method to return html page from given url as string
    """
    try:
        # if good page encoding
        return urlopen(url).read().decode()
    except UnicodeDecodeError:
        # if windows page encoding
        return urlopen(url).read().decode('cp1251')


def get_div_inner_html(page_as_string):
    """
    Method for take all divs inner html from string
    """

    # make list of chars from page-string
    page_as_list = list(page_as_string)
    # we would start from end of page
    page_as_list.reverse()
    # define needed variables
    inner_html = []
    end_div_counter = 0
    left_index = 0

    for index in range(0, len(page_as_list)):
        # current char of page
        char = page_as_list[index]

        # if we have div close tag before current char, increase counter
        if (
            page_as_list[index - 1] == '<' and
            page_as_list[index - 2] == '/' and
            page_as_list[index - 3] == 'd'
        ):
            end_div_counter += 1

        # check for open-div-tag
        if char == '>':
            left_index = 1
            current_char = char
            while current_char != '<':
                current_char = page_as_list[index + left_index]
                left_index += 1

            # if '>' close open-div-tag, decrease counter
            if page_as_list[index + left_index - 2] == 'd':
                end_div_counter -= 1
            else:
                left_index = 0

        # if current char in open-div-tag, don't copy current char
        if left_index != 0:
            left_index -= 1
            continue

        # if current char not in div, don't copy current char
        if end_div_counter != 0:
            inner_html.append(char)

    # reverse chars back (to normal sequence)
    inner_html.reverse()
    # make string from list
    inner_html_string = ''.join(inner_html).replace('</div>', '')

    return inner_html_string


if __name__ == '__main__':
    url = 'https://www.google.ru/'
    page_as_string = get_page(url)
    print(get_div_inner_html(page_as_string))