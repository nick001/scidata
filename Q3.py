class Animal:
    def __init__(self, name='anonymous'):
        self.name = name.capitalize()
        self.hello_message = 'Hello! I am wild animal.'

    def speak(self):
        return '{} My name is {}.'.format(self.hello_message, self.name)


class Cat(Animal):
    def __init__(self, name=''):
        super().__init__(name)
        self.hello_message = 'Mew-mew'

    def speak(self):
        return 'Cat {}. {}'.format(self.name, self.hello_message)


if __name__ == '__main__':
    animal = Animal('bear')
    print(animal.speak())
    cat = Cat('fuzzy')
    print(cat.speak())
    animal = Animal()
    print(animal.speak())