from functools import reduce


# Simplest generator
# That does not make list!
test_sequence = range(-50, 55, 5)
print('Input sequence : {}'.format(list(test_sequence)))

# Use map to power all elements
map_result = list(map(lambda x: x ** 2, test_sequence))
print('Power 2        : {}'.format(map_result))

# Use filter to delete uneven elements
filter_result = list(filter(lambda x: x % 2 == 0, map_result))
print('Even elements  : {}'.format(filter_result))

# Use reduce to find sum of remaining elements
reduce_result = reduce(lambda x, y: x + y, filter_result)
print('Get sum        : {}'.format(reduce_result))