def check_empty_list_decorator(func):
    """
    Decorator for catch empty function input.
    If empty print message and return None.
    """

    def function_wrapper(x):
        if len(x) == 0:
            print('You try to find mean value of empty list')
            return None
        else:
            return func(x)

    return function_wrapper


@check_empty_list_decorator
def mean_value(list_of_values):
    """
    Method for calculate mean value of list values.
    Return None if input list empty

    :param list_of_values: list of value to found mean value
    :return: list mean value
    """

    """
    We no need to use that code in that method, because we use decorator.
    So that method is easy for undestanding.

    if len(list_of_values) == 0:
        print('You try to find mean value of empty list')
        return None
    """

    # return mean value
    return sum(list_of_values) / len(list_of_values)


if __name__ == '__main__':
    list_of_values = [1, 2, 3, 3, 2, 1]
    print(mean_value(list_of_values))